package au.com.sumo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OpsToolsApplication {

	public static void main(String[] args) {
		SpringApplication.run(OpsToolsApplication.class, args);
	}

}
