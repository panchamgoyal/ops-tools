package au.com.sumo.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import au.com.sumo.util.ErrorConstants;
import au.com.sumo.util.FileUtil;

@RestController
public class FileController {

	@RequestMapping(value = "/file/csv", method = RequestMethod.POST)
	ResponseEntity<?> uploadcsv(@RequestParam("file") MultipartFile multipartFile, HttpServletRequest request) {
		try {
			if (multipartFile.isEmpty()) {
				return new ResponseEntity<Object>(ErrorConstants.EMPTY_FILE, new HttpHeaders(), HttpStatus.BAD_REQUEST);
			} else if (!FileUtil.getExtension(multipartFile.getOriginalFilename()).equalsIgnoreCase("csv")) {
				return new ResponseEntity<Object>(ErrorConstants.NOT_CSV_FILE, new HttpHeaders(),
						HttpStatus.BAD_REQUEST);
			}
			return new ResponseEntity<>("File Uploaded",HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>("File Not Uploaded",HttpStatus.INTERNAL_SERVER_ERROR);
		}
		

	}
}
