package au.com.sumo.util;

public class ErrorConstants {


	public static final String EMPTY_FILE = "CSV file is empty.";
	public static final String NOT_CSV_FILE = "File is not an CSV file.";

}