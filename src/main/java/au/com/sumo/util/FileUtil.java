package au.com.sumo.util;

import java.math.BigInteger;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.security.SecureRandom;

public class FileUtil {

	private static SecureRandom random = new SecureRandom();

	public static String getExtension(String fileName) {
		return fileName.split("\\.(?=[^\\.]+$)")[1];
	}

	public static String removeExtension(String fileName) {
		return fileName.split("\\.(?=[^\\.]+$)")[0];
	}

	public static String[] splitExtension(String fileName) {
		return fileName.split("\\.(?=[^\\.]+$)");
	}

	public static String getMimeType(String fileName) {

		FileNameMap mimeTypes = URLConnection.getFileNameMap();
		String mimeType = mimeTypes.getContentTypeFor(fileName);
		return mimeType;
	}

	public String generateFileName(String fileName) {
		String fileNameKey = String.format("%s-%s-%s", getRandomGeneratedId(), getRandomGeneratedId(), fileName);
		return fileNameKey;
	}

	public static String getRandomGeneratedId() {
		return new BigInteger(130, random).toString(32);
	}

}